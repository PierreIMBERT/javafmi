#!/bin/bash
# add the licence header to all java sources, the header file is passed as argument
# if there are a licence, doesn't add anything

function fileHasCopyright {
    local file=$1
    local copyrightLine=$(grep -n "Copyright" $file | awk -F: '{print $1}')
    if [[ $copyrightLine -gt 0 ]]
    then
        local return=true
    else
        local return=false
    fi
    echo $return
}

header=$1

for file in $(find ./ -name "*.java")
do
    hasCopyright=$(fileHasCopyright $file)
    if (( $hasCopyright == "false" ))
    then
	   cat $header > $file.temp
	   cat $file >> $file.temp
	   mv $file.temp $file
    fi
done