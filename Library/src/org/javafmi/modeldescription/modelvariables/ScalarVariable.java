/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.javafmi.modeldescription.modelvariables;

import org.javafmi.modeldescription.typedefinitions.BooleanType;
import org.javafmi.modeldescription.typedefinitions.EnumerationType;
import org.javafmi.modeldescription.typedefinitions.IntegerType;
import org.javafmi.modeldescription.typedefinitions.RealType;
import org.javafmi.modeldescription.typedefinitions.StringType;
import org.javafmi.modeldescription.typedefinitions.Type;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

public class ScalarVariable {

    @Attribute
    private String name;
    @Attribute
    private int valueReference;
    @Attribute(required = false)
    private String description;
    @Attribute(required = false)
    private String variability;
    @Attribute(required = false)
    private String causality;
    @Attribute(required = false)
    private String alias;
    @Element(name = "Real", required = false)
    private RealType realType;
    @Element(name = "Integer", required = false)
    private IntegerType integerType;
    @Element(name = "Boolean", required = false)
    private BooleanType booleanType;
    @Element(name = "String", required = false)
    private StringType stringType;
    @Element(name = "Enumeration", required = false)
    private EnumerationType enumerationType;
    @Element(name = "DirectDependency", required = false)
    private DirectDependency direcDependency;

    public String getName() {
        return name;
    }

    public int getValueReference() {
        return valueReference;
    }

    public String getDescription() {
        return description;
    }

    public String getVariability() {
        return variability;
    }

    public String getCausality() {
        return causality;
    }

    public String getAlias() {
        return alias;
    }

    public DirectDependency getDirecDependency() {
        return direcDependency;
    }

    public Type getType() {
        if (realType != null)
            return realType;
        if (integerType != null)
            return integerType;
        if (booleanType != null)
            return booleanType;
        if (stringType != null)
            return stringType;
        if (enumerationType != null)
            return enumerationType;
        return null;
    }
}
