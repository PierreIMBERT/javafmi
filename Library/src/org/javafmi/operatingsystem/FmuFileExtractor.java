/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.javafmi.operatingsystem;

import java.io.File;

public class FmuFileExtractor {

    private final String modelDescriptionFilename = "modelDescription.xml";
    private String workingDirectory;
    private Unzipper unzipper;

    public FmuFileExtractor(File fmuFile) {
        this.workingDirectory = buildWorkingDirectory(fmuFile);
        this.unzipper = new Unzipper(fmuFile);
        deleteAllExceptFmus(new File(workingDirectory));
    }

    private String buildWorkingDirectory(File fmuFile) {
        return PathBuilder.buildPath(new String[]{fmuFile.getParentFile().getName()}, "tmp");
    }

    private void deleteAllExceptFmus(File file) {
        if (file.isDirectory()) {
            deleteFilesInside(file);
            file.delete();
        } else if (file.getName().endsWith(".fmu")) return;
        else file.delete();
    }

    private void deleteFilesInside(File directory) {
        for (File child : directory.listFiles())
            deleteAllExceptFmus(child);
    }

    public File getModelDescriptionFile() {
        return unzipper.unzip(modelDescriptionFilename, workingDirectory);
    }

    public String getLibraryPath(String modelName) {
        return getLibraryFile(modelName).getPath();
    }

    public File getLibraryFile(String modelName) {
        String operatingSystem = System.getProperty("os.name").toLowerCase();
        String architecture = System.getProperty("os.arch").toLowerCase();
        String binariesFolder = "binaries";
        String libraryExtension = "";
        String libraryFolder = "";
        if (operatingSystem.startsWith("mac")) {
            libraryExtension = ".dylib";
            libraryFolder = "darwin";
        } else if (operatingSystem.startsWith("win")) {
            libraryExtension = ".dll";
            libraryFolder = "win";
        }

        if (architecture.contains("64"))
            architecture = "64";
        else
            architecture = "32";
        String libraryPathInsideZip = PathBuilder.buildPath(new String[]{binariesFolder, libraryFolder + architecture}, modelName + libraryExtension);
        return unzipper.unzip(libraryPathInsideZip, workingDirectory);
    }
}
