/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.javafmi;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.javafmi.modeldescription.modelvariables.ScalarVariable;
import org.javafmi.modeldescription.typedefinitions.BooleanType;
import org.javafmi.modeldescription.typedefinitions.EnumerationType;
import org.javafmi.modeldescription.typedefinitions.IntegerType;
import org.javafmi.modeldescription.typedefinitions.RealType;
import org.javafmi.modeldescription.typedefinitions.StringType;
import org.javafmi.wrapper.FmuProxy;

public class VariableReader {

    private static VariableReader instance;
    private static final Map<Class, Method> getterMethodsByClass;

    static {
        getterMethodsByClass = new HashMap<>();
        try {
            getterMethodsByClass.put(RealType.class, FmuProxy.class.getMethod("getReal", Integer.class));
            getterMethodsByClass.put(IntegerType.class, FmuProxy.class.getMethod("getInteger", Integer.class));
            getterMethodsByClass.put(BooleanType.class, FmuProxy.class.getMethod("getBoolean", Integer.class));
            getterMethodsByClass.put(StringType.class, FmuProxy.class.getMethod("getString", Integer.class));
        } catch (NoSuchMethodException | SecurityException ex) {
            Logger.getLogger(FmuProxy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private VariableReader() {
    }

    public static VariableReader getInstance() {
        if (instance == null)
            instance = new VariableReader();
        return instance;
    }

    public Variable read(String variableName, FmuProxy fmuProxy) {
        ScalarVariable modelVariable = fmuProxy.getModelDescription().getScalarVariable(variableName);
        if (isEnumeration(modelVariable))
            return buildEnumerationVariable(modelVariable, fmuProxy);
        if (isConstant(modelVariable))
            return new Variable(variableName, modelVariable.getType().getStart());
        else
            return buildVariable(modelVariable, fmuProxy);
    }

    private boolean isConstant(ScalarVariable modelVariable) {
        final String variability = modelVariable.getVariability();
        if (variability == null) return false;
        return variability.equalsIgnoreCase("constant");
    }

    private boolean isEnumeration(ScalarVariable modelVariable) {
        return modelVariable.getType() instanceof EnumerationType;
    }

    private Variable buildEnumerationVariable(ScalarVariable modelVariable, FmuProxy fmuProxy) {
        return new Variable(modelVariable.getName(), fmuProxy.getEnumeration(modelVariable));
    }

    private Variable buildVariable(ScalarVariable modelVariable, FmuProxy fmuProxy) {
        Object variableValue = null;
        try {
            variableValue = getterMethodsByClass.get(modelVariable.getType().getClass()).invoke(fmuProxy, modelVariable.getValueReference());
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(VariableReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new Variable(modelVariable.getName(), variableValue);
    }
}
