/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package fmu.tester;

import org.fmu.tester.Command;
import org.fmu.tester.CommandFactory;
import org.fmu.tester.command.CommandGet;
import org.fmu.tester.command.CommandHelp;
import org.fmu.tester.command.CommandInit;
import org.fmu.tester.command.CommandLog;
import org.fmu.tester.command.CommandMultiStep;
import org.fmu.tester.command.CommandReset;
import org.fmu.tester.command.CommandSet;
import org.fmu.tester.command.CommandStep;
import org.fmu.tester.command.CommandTerminate;
import static org.junit.Assert.*;
import org.junit.Test;

public class CommandFactoryTest {

    @Test
    public void returnsGetCommand() {
        Command command = getCommand("get var");
        assertEquals(CommandGet.class, command.getClass());
        assertEquals("get <varname>", CommandGet.getUsage());
    }

    @Test
    public void returnsHelpCommand() {
        Command command = getCommand("help");
        assertEquals(CommandHelp.class, command.getClass());
        assertEquals("help", CommandHelp.getUsage());
    }

    @Test
    public void returnsInitCommand() {
        Command command = getCommand("init");
        assertEquals(CommandInit.class, command.getClass());
        assertEquals("init", CommandInit.getUsage());
    }

    @Test
    public void returnsLogCommand() {
        Command command = getCommand("log var");
        assertEquals(CommandLog.class, command.getClass());
        assertEquals("log <varname>", CommandLog.getUsage());
    }

    @Test
    public void returnsMultiStepCommand() {
        Command command = getCommand("multistep 1 1");
        assertEquals(CommandMultiStep.class, command.getClass());
        assertEquals("multistep <step size> <number of steps>", CommandMultiStep.getUsage());
    }

    @Test
    public void returnsResetCommand() {
        Command command = getCommand("reset");
        assertEquals(CommandReset.class, command.getClass());
        assertEquals("reset", CommandReset.getUsage());
    }

    @Test
    public void returnsSetCommand() {
        Command command = getCommand("set var 1");
        assertEquals(CommandSet.class, command.getClass());
        assertEquals("set <varname> <value>", CommandSet.getUsage());
    }

    @Test
    public void returnsStepCommand() {
        Command command = getCommand("step 1");
        assertEquals(CommandStep.class, command.getClass());
        assertEquals("step <step size>", CommandStep.getUsage());
    }

    @Test
    public void returnsTerminateCommand() {
        Command command = getCommand("terminate");
        assertEquals(CommandTerminate.class, command.getClass());
        assertEquals("terminate", CommandTerminate.getUsage());
    }

    private Command getCommand(String command) {
        return CommandFactory.createCommand(command, null);
    }
}