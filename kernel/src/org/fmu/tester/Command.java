/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmu.tester;

public abstract class Command {

    public static final String GET = "get";
    public static final String HELP = "help";
    public static final String INIT = "init";
    public static final String LOG = "log";
    public static final String MULTI_STEP = "multistep";
    public static final String RESET = "reset";
    public static final String SET = "set";
    public static final String STEP = "step";
    public static final String TERMINATE = "terminate";
    private final VariableLoggerList variableLoggerList;

    public Command(VariableLoggerList variableLoggerList) {
        this.variableLoggerList = (variableLoggerList == null ? new VariableLoggerList() : variableLoggerList);
    }

    public VariableLoggerList getVariableLoggerList() {
        return variableLoggerList;
    }

    public abstract String execute(Simulation simulation);
}
