/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmu.tester;

import java.util.List;
import org.javafmi.Status;
import org.javafmi.Variable;

public class Simulation {

    private final org.javafmi.Simulation simulation;
    private double time;
    private boolean finished;

    public Simulation(org.javafmi.Simulation simulation) {
        this.simulation = simulation;
        this.time = 0;
        this.finished = false;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public Status cancelStep() {
        return simulation.cancelStep();
    }

    public Status doStep(double stepSize) {
        time += stepSize;
        return simulation.doStep(stepSize);
    }

    public boolean equals(Object arg0) {
        return simulation.equals(arg0);
    }

    public int hashCode() {
        return simulation.hashCode();
    }

    public Status init(double startTime, double stopTime) {
        return simulation.init(startTime, stopTime);
    }

    public Status init(double startTime) {
        return simulation.init(startTime);
    }

    public boolean isDoingLogging() {
        return simulation.isDoingLogging();
    }

    public boolean isInstanceInteractive() {
        return simulation.isInstanceInteractive();
    }

    public boolean isInstanceVisible() {
        return simulation.isInstanceVisible();
    }

    public Variable<?> readVariable(String variableName) {
        return simulation.readVariable(variableName);
    }

    public Status resetSlave() {
        return simulation.resetSlave();
    }

    public Status terminate() {
        this.finished = true;
        return simulation.terminate();
    }

    @Override
    public String toString() {
        return simulation.toString();
    }

    public Status writeVariable(String name, Boolean value) {
        return simulation.writeVariable(name, value);
    }

    public Status writeVariable(String name, Double value) {
        return simulation.writeVariable(name, value);
    }

    public Status writeVariable(String name, Integer value) {
        return simulation.writeVariable(name, value);
    }

    public Status writeVariable(String name, String value) {
        return simulation.writeVariable(name, value);
    }

    public List<String> listVariables() {
        return simulation.listVariables();
    }

    public boolean isFinished() {
        return finished;
    }

    String getInternalName() {
        return simulation.getInternalName();
    }
}
