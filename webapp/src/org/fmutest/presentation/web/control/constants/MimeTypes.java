/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmutest.presentation.web.control.constants;

import java.util.HashMap;

public class MimeTypes {

    private HashMap<String, String> hmMimeTypes;
    private static MimeTypes oInstance;
    private static final String DEFAULT_MIME_TYPE = "image/jpeg";

    private MimeTypes() {
        this.hmMimeTypes = new HashMap<String, String>();
        this.hmMimeTypes.put("image/bmp", "jpg");
        this.hmMimeTypes.put("image/gif", "gif");
        this.hmMimeTypes.put("image/jpeg", "jpg");
        this.hmMimeTypes.put("image/png", "png");
        this.hmMimeTypes.put("image/tiff", "jpg");
    }

    public synchronized static MimeTypes getInstance() {
        if (oInstance == null) oInstance = new MimeTypes();
        return oInstance;
    }

    public String get(String contentType) {
        if (!this.hmMimeTypes.containsKey(contentType)) contentType = MimeTypes.DEFAULT_MIME_TYPE;
        return this.hmMimeTypes.get(contentType);
    }
}