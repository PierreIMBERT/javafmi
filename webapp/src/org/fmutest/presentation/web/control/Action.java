/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmutest.presentation.web.control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.minidev.json.JSONObject;
import org.fmutest.javafmi.KernelConnection;

public abstract class Action {

    protected String codeLanguage;
    protected HttpServletRequest request;
    protected HttpServletResponse response;
    protected ActionFactory actionsFactory;

    public Action() {
        this.codeLanguage = null;
        this.request = null;
        this.response = null;
        this.actionsFactory = ActionFactory.getInstance();
    }

    protected void initLanguage() {
        this.codeLanguage = null;
        if (this.codeLanguage == null)
            this.codeLanguage = this.request.getHeader("Accept-Language").substring(0, 2);
    }

    protected JSONObject getFMUInfo(String fmuId, KernelConnection kernelConnection) {
        JSONObject fmuInfo;
        fmuInfo = new JSONObject();
        fmuInfo.put("id", fmuId);
        fmuInfo.put("modelName", kernelConnection.getModelName());
        fmuInfo.put("commands", kernelConnection.getCommands());
        fmuInfo.put("variables", kernelConnection.getVariables());
        return fmuInfo;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    public void initialize() {
        this.response.setContentType("text/html;charset=UTF-8");
        this.initLanguage();
    }

    public abstract String execute();
}