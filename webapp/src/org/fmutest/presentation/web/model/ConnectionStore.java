/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmutest.presentation.web.model;

import java.util.HashMap;
import org.fmutest.javafmi.KernelConnection;

public class ConnectionStore {

    private HashMap<String, KernelConnection> interpreterMap;
    private static ConnectionStore instance;

    private ConnectionStore() {
        this.interpreterMap = new HashMap<String, KernelConnection>();
    }

    public static ConnectionStore getInstance() {
        if (instance == null)
            instance = new ConnectionStore();
        return instance;
    }

    public boolean exists(String key) {
        return interpreterMap.containsKey(key);
    }

    public KernelConnection get(String key) {
        return interpreterMap.get(key);
    }

    public void register(String key, KernelConnection interpreter) {
        interpreterMap.put(key, interpreter);
    }

    public void unregister(String key) {
        if (!interpreterMap.containsKey(key))
            return;
        interpreterMap.remove(key);
    }
}