/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmutest.javafmi;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.fmu.tester.Kernel;
import org.fmu.tester.command.CommandHelp;
import org.fmutest.presentation.web.library.LibraryFile;

public class KernelConnection {

    private Kernel kernel;
    private File workingDirectory;

    public KernelConnection(String id, InputStream fmu) {
        this.workingDirectory = new File(id);
        workingDirectory.mkdirs();
        File fmuFile = getFmuFile();
        LibraryFile.writeFile(fmuFile, fmu);
        this.kernel = new Kernel(fmuFile.getAbsolutePath());
    }

    private File getFmuFile() {
        return new File(workingDirectory, "uploaded.fmu");
    }

    public CommandResult execute(String command) {
        String result = kernel.executeCommand(command);
        if (result.startsWith("Error:"))
            return new ErrorResult(command, result);
        return new OkResult(command, result);

    }

    public void terminate() {
        kernel.executeCommand("terminate");
        LibraryFile.deleteAll(workingDirectory);
    }

    public CommandResult load() {
        if (kernel == null)
            return new ErrorResult("", "Problem loading FMU");
        return new OkResult("", "");
    }

    public String getModelName() {
        return kernel.getModelName();
    }

    public String[] getCommands() {
        String[] split = new CommandHelp(null).execute(null).split("\n");
        return split;
    }

    public String[] getVariables() {
        return kernel.listVariables();
    }

    public List<File> getLogs() {
        return LogLoader.load(workingDirectory);
    }

    public List<String> getLogsNames() {
        if (kernel.isTerminated())
            return LogLoader.getLogNames(workingDirectory);
        return new ArrayList<String>();
    }

    public DownloadableBuilder getDownloadableLogs() {
        return new DownloadableBuilder(getLogs());
    }

    private static class ErrorResult extends CommandResult {

        public ErrorResult(String command, String errorMessage) {
            super(command, errorMessage, true);
        }
    }

    private static class OkResult extends CommandResult {

        public OkResult(String command, String message) {
            super(command, message, false);
        }
    }
}
